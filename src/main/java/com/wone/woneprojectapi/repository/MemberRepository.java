package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface MemberRepository extends JpaRepository <Member, Long> {
    // id 중복확인
    // userId = unique
    // userid와 일치하는 갯수를 알고 싶을 때
    long countByUserId(String userId);

    List<Member> countByJoinDate(LocalDate joinDate);


}
