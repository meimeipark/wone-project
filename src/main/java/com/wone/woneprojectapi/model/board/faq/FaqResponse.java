package com.wone.woneprojectapi.model.board.faq;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class FaqResponse {
    private Long id;
    private String memberName;
    private LocalDateTime askCreateDate;
    private String askContent;
    private String publicType;
    private String comment;
    private Short askPassword;
}
