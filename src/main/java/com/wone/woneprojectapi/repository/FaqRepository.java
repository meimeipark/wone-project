package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Board;
import com.wone.woneprojectapi.entity.Faq;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.board.faq.FaqItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FaqRepository extends JpaRepository <Faq, Long>{
    List<Faq> findAllByMemberOrderByIdDesc(Member member);
}
