package com.wone.woneprojectapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberCountResponse {
    private Integer sevenDaysAgo;
    private Integer sixDaysAgo;
    private Integer fiveDaysAgo;
    private Integer todayTotal;
}
