package com.wone.woneprojectapi.model.challenge;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ChallengeResponse {
    private Long id;
    private Long memberId;
    private LocalDate challengeDate;
    private Double goalsMoney;
}
