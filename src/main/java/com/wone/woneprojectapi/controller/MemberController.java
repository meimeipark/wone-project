package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.model.member.*;
import com.wone.woneprojectapi.service.MemberService;
import com.wone.woneprojectapi.service.ResponseService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;


    // 사용자 회원가입
    @PostMapping("/join")
    public CommonResult setMemberJoin(@Valid @RequestBody MemberJoinRequest request) throws Exception {
        memberService.setMemberJoin(request);
        return ResponseService.getSuccessResult();
    }

    // 회원가입 id 중복 체크
    @GetMapping("/check/id")
    public MemberDupCheckResponse getMemberDupCheck(
            @RequestParam(name = "userId") String username){
        return memberService.getMemberIdDupCheck(username);
    }

    // WEB 사용자 전체 조회
    @GetMapping("/all")
    public ListResult<MemberItem> getMembers(){
        return ResponseService.getListResult(memberService.getMembers());
    }

    // WEB 관리자 페이지 사용자 상세 조회
    @GetMapping("/detail/admin/{id}")
    public SingleResult<MemberResponse> getMemberAdmin(@PathVariable long id){
        return ResponseService.getSingleResult(memberService.getMemberAdmin(id));
    }

    // APP 사용자 상세 조회
    @GetMapping("/detail/{id}")
    public SingleResult<MemberResponse> getMember(@PathVariable long id){
        return ResponseService.getSingleResult(memberService.getMember(id));
    }

    // 사용자 토탈 조회
    @GetMapping("/total/count")
    public SingleResult<MemberCountResponse> getMemberCount(@PathVariable LocalDate joindate){
        return ResponseService.getSingleResult(memberService.getMemberCount(joindate));
    }

    // APP 사용자 정보 수정
    @PutMapping("/info/change/{id}")
    public CommonResult putMemberInfo(@PathVariable long id, @RequestBody MemberInfoChangeRequest request){
        memberService.putMemberInfo(id, request);
        return ResponseService.getSuccessResult();
    }

    // APP 사용자 탈퇴
    @PutMapping("/out/{id}")
    public CommonResult putMemberOut(@PathVariable long id, @RequestBody MemberOutChangeRequest request){
        memberService.putMemberOut(id, request);
        return ResponseService.getSuccessResult();
    }
}
