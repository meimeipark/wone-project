package com.wone.woneprojectapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CategoryType {
    SEND_MONEY("", "송금"),
    STUDY("", "교육"),
    MEAL("", "식사"),
    COFFEE("", "커피구매"),
    TRANSPORT("", "대중교통"),
    SHOPPING("", "쇼핑"),
    BILL("", "공과금"),
    MEDICAL("", "의료비"),
    CULTURAL("", "문화생활"),
    MEETING("", "모임"),
    SUBSCRIPTION("", "구독료"),
    ETC_PAY("", "기타");

    private final String categoryImgUrl;
    private final String categoryName;
}
