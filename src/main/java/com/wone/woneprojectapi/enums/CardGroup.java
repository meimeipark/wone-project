package com.wone.woneprojectapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CardGroup {
    NH("농협"),
    KB("국민"),
    SH("신한"),
    IBK("기업"),
    WOORI("우리"),
    KAKAO("카카오"),
    TOSS("토스"),
    HN("하나"),
    ETC("기타");

    private final String cardCompany;
}
