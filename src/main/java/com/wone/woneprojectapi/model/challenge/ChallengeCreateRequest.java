package com.wone.woneprojectapi.model.challenge;

import com.wone.woneprojectapi.entity.Member;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ChallengeCreateRequest {
    private LocalDate challengeDate;
    private Double goalsMoney;
}
