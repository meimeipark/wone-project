package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.model.challenge.ChallengeCreateRequest;
import com.wone.woneprojectapi.model.challenge.ChallengeGoalsMoneyChangeRequest;
import com.wone.woneprojectapi.model.challenge.ChallengeItem;
import com.wone.woneprojectapi.model.challenge.ChallengeResponse;
import com.wone.woneprojectapi.service.ChallengeService;
import com.wone.woneprojectapi.service.MemberService;
import com.wone.woneprojectapi.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/challenge")
public class ChallengeController {
    private final MemberService memberService;
    private final ChallengeService challengeService;

    @PostMapping("/member-id/{memberId}")
    public CommonResult setChallenge(@PathVariable long memberId, @RequestBody ChallengeCreateRequest request){
        Member member = memberService.getData(memberId);
        challengeService.setChallenge(member, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/goals-money/all/member-id/{memberId}")
    public ListResult<ChallengeItem> getChallengeMember(@PathVariable long memberId){
        Member member = memberService.getData(memberId);
        return ResponseService.getListResult(challengeService.getChallengeMember(member));

    }

    @GetMapping("/challenge-goals-money/detail/{id}")
    public SingleResult<ChallengeResponse> getChallenge(@PathVariable long id){
        return ResponseService.getSingleResult(challengeService.getChallenge(id));
    }

    @PutMapping("/challenge-goals-money/{id}")
    public CommonResult putChallenge(@PathVariable long id, @RequestBody ChallengeGoalsMoneyChangeRequest request){
        challengeService.putChallenge(id, request);

        return ResponseService.getSuccessResult();
    }
}
