package com.wone.woneprojectapi.entity;

import com.wone.woneprojectapi.enums.MemberGroup;
import com.wone.woneprojectapi.enums.MemberStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15, unique = true)
    private String userId;

    @Column(nullable = false, length = 20)
    private String username;

    @Column(nullable = false)
    private LocalDate birthDate;

    @Column(nullable = false, length = 20)
    private String password;

    @Column(nullable = false)
    private LocalDateTime joinDate;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private MemberGroup memberGroup;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private MemberStatus memberStatus;

    private LocalDateTime outDate;
}
