package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Board;
import com.wone.woneprojectapi.entity.Faq;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.enums.BoardType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BoardRepository extends JpaRepository<Board, Long> {

    List<Board> findAllByBoardTypeEqualsOrderByIdDesc(BoardType boardType);
}
