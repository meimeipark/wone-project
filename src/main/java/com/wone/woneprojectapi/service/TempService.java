package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.lib.CommonFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Service
public class TempService {
    public void setMemberListByFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.multipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        // 푸슝파싱부분

        bufferedReader.close();
    }
}
