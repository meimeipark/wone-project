package com.wone.woneprojectapi.model.card;

import com.wone.woneprojectapi.enums.CardGroup;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CardGroupChangeRequest {
    @Enumerated(value = EnumType.STRING)
    private CardGroup cardGroup;
}
