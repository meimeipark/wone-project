package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.model.board.faq.*;
import com.wone.woneprojectapi.service.FaqService;
import com.wone.woneprojectapi.service.MemberService;
import com.wone.woneprojectapi.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/faq")
public class FaqController {
    private final MemberService memberService;
    private final FaqService faqService;

    @PostMapping("/new/member-id/{memberId}")
    public CommonResult setFaq (@PathVariable long memberId, @RequestBody FaqCreateRequest request){
        Member member = memberService.getData(memberId);
        faqService.setFaq(member, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/list/admin")
    public ListResult<FaqAdminItem> getFaqsAdmin (){
        return ResponseService.getListResult(faqService.getFaqsAdmin());
    }

    @GetMapping("/list/app")
    public ListResult<FaqAppItem> getFaqsApp (){
        return ResponseService.getListResult(faqService.getFaqsApp());
    }

    @GetMapping("/detail/{id}")
    public SingleResult<FaqResponse> getFaq(@PathVariable long id){
        return ResponseService.getSingleResult(faqService.getFaq(id));
    }

    @GetMapping("/contents/member-id/{memberId}")
    public ListResult<FaqItem> getFaqUser (@PathVariable long memberId) {
        Member member = memberService.getData(memberId);

        return ResponseService.getListResult(faqService.getFaqUser(member));
    }

    @PutMapping("/comment/{id}")
    public CommonResult putFaqAdmin(@PathVariable long id, @RequestBody FaqAdminChangeRequest request){
        faqService.putFaqAdmin(id, request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/change-faq/{id}")
    public CommonResult putFaqUser(@PathVariable long id, @RequestBody FaqUserChangeRequest request ){
        faqService.putFaqUser(id, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{id}")
    public CommonResult delFaq(@PathVariable long id){
        faqService.delFaq(id);

        return ResponseService.getSuccessResult();
    }
}
