package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.model.board.notice.*;
import com.wone.woneprojectapi.service.BoardService;
import com.wone.woneprojectapi.service.MemberService;
import com.wone.woneprojectapi.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;
    private final MemberService memberService;

    @PostMapping("/new/member-id/{memberId}")
    public CommonResult setBoard (@PathVariable long memberId, @RequestBody BoardCreateRequest request){
        Member member = memberService.getData(memberId);
        boardService.setBoard(member, request);

        return ResponseService.getSuccessResult();
    }

//    @GetMapping("/notice/list")
//    public ListResult<BoardNoticeItem> getNoticeBoards (){
//        return ResponseService.getListResult(boardService.getNoticeBoards());
//    }

    @GetMapping("/notice/all")
    public ListResult<BoardNoticeItem> getNotices (){
        return ResponseService.getListResult(boardService.getNotices());
    }

    @GetMapping("/use/all")
    public ListResult<BoardUseItem> getUseBoards (){
        return ResponseService.getListResult(boardService.getUseBoards());
    }


    @GetMapping("/detail/notice/{id}")
    public SingleResult<BoardNoticeResponse> getNoticeBoard(@PathVariable long id){
        return ResponseService.getSingleResult(boardService.getNoticeBoard(id));
    }

    @GetMapping("/detail/use/{id}")
    public SingleResult<BoardUseResponse> getUesBoard(@PathVariable long id){
        return ResponseService.getSingleResult(boardService.getUseBoard(id));
    }

    @PutMapping("/change-content/{id}")
    public CommonResult putBoard(@PathVariable long id, @RequestBody BoardContentChangeRequest request){
        boardService.putBoard(id, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{id}")
    public CommonResult delBoard(@PathVariable long id){
        boardService.delBoard(id);

        return ResponseService.getSuccessResult();
    }

}
