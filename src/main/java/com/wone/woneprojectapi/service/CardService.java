package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Card;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.card.*;
import com.wone.woneprojectapi.repository.CardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class CardService {
    private final CardRepository cardRepository;

    /**
     * 카드 등록
     * @param member 별로
     * @param request 카드사(enums에 없는 기타 카드사), 카드번호, 유효기간, cvc 카드 신규 등록
     */
    // 카드 등록
    public void setCard(Member member, CardAddRequest request){
        Card addData = new Card();
        addData.setMember(member);
        addData.setCardGroup(request.getCardGroup());
        addData.setCardNumber(request.getCardNumber());
        addData.setEndDate(request.getEndDate());
        addData.setCvc(request.getCvc());
        addData.setEtcMemo(request.getEtcMemo());

        cardRepository.save(addData);
    }

//    /**
//     *
//     * @param member memberId
//     * @return true 등록 가능 / false 등록 불가능
//     */
//    // card 등록 시 memberId 중복확인을 참거짓으로 표현해라
//    // memberId를 확인 시 이미 등록되어 있으면 2장 등록 불가
//    private boolean cardCheckMemberId(Member member){
//        long dupCount = cardRepository.countByMember(member);
//
//        return dupCount <= 0;
//    }
//
//    public CardDupCheckResponse getCardCheck(Member member){
//        CardDupCheckResponse response = new CardDupCheckResponse();
//        response.setCardCheck(cardCheckMemberId(member));
//
//        return response;
//    }

    /**
     * 카드 상세 조회
     * @param id 카드Id
     * @return 등록된 카드 상세 조
     */

    // 카드 상세조회
    public CardResponse getCard(long id){
        Card originData = cardRepository.findById(id).orElseThrow();
        CardResponse response = new CardResponse();
        response.setId(originData.getId());
        response.setMemberId(originData.getMember().getId());
        response.setCardGroup(originData.getCardGroup().getCardCompany());
        response.setCardNumber(originData.getCardNumber());
        response.setEndDate(originData.getEndDate());
        response.setCvc(originData.getCvc());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    /**
     * 멤버별로 카드 조회
     * @param member 멤버Id별로
     * @return 카드 조회(APP에서 멤버 별로 조회)
     */
    public List<CardItem> getCardMembers(Member member){
        List<Card> originList = cardRepository.findAllByMember(member);
        List<CardItem> result = new LinkedList<>();
        for (Card card : originList) {
            CardItem addItem = new CardItem();
            addItem.setId(card.getId());
            addItem.setMemberId(card.getMember().getId());
            addItem.setCardGroup(card.getCardGroup().getCardCompany());
            addItem.setCardNumber(card.getCardNumber());
            addItem.setEndDate(card.getEndDate());
            addItem.setCvc(card.getCvc());
            addItem.setEtcMemo(card.getEtcMemo());

            result.add(addItem);
        }
        return result;
    }

    /**
     * 카드 수정
     * @param id
     * @param request
     */
    // 카드 수정
    public void putCardChange(long id, CardChangeRequest request){
        Card originData = cardRepository.findById(id).orElseThrow();
        originData.setCardNumber(request.getCardNumber());
        originData.setEndDate(request.getEndDate());
        originData.setCvc(request.getCvc());
        originData.setEtcMemo(request.getEtcMemo());

        cardRepository.save(originData);
    }

    /**
     * 등록된 카드사 수정
     * @param id
     * @param request
     */
    // 카드사 수정
    public void putCardGroupChange(long id, CardGroupChangeRequest request){
        Card originData = cardRepository.findById(id).orElseThrow();
        originData.setCardGroup(request.getCardGroup());

        cardRepository.save(originData);
    }

    /**
     * 등록된 카드 삭제
     * @param id
     */
    // 카드 삭제
    public void delCard(long id){
        cardRepository.deleteById(id);
    }
}
