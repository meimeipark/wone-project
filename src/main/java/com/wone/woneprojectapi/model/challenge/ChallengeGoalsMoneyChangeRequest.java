package com.wone.woneprojectapi.model.challenge;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChallengeGoalsMoneyChangeRequest {
    private Double goalsMoney;
}
