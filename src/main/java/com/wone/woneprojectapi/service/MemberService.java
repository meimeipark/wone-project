package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.enums.MemberGroup;
import com.wone.woneprojectapi.enums.MemberStatus;
import com.wone.woneprojectapi.enums.ResultCode;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.member.*;
import com.wone.woneprojectapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class MemberService {
    private final MemberRepository memberRepository;

    /**
     *
     * @param id memberId를 받아
     * @return 조인
     */
    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }


    /**
     * 회원 가입 시킨
     *
     * @param request 회원 가입 창에서 고객이 작성한 정보
     * @throws Exception 아이디 중복일 경우, 비밀번호와 비밀번호 확인이 일치 하지 않을 경우
     *
     */
    // 회원 가입
    public void setMemberJoin(MemberJoinRequest request) throws Exception {
        if (!isNewUserId(request.getUserId())) throw new Exception();
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception();
        Member member = new Member();
        member.setUserId(request.getUserId());
        member.setUsername(request.getUsername());
        member.setBirthDate(request.getBirthDate());
        member.setPassword(request.getPassword());
        member.setJoinDate(LocalDateTime.now());
        member.setMemberGroup(MemberGroup.USER_MEMBER);
        member.setMemberStatus(MemberStatus.NORMAL);

        memberRepository.save(member);
    }


    /**
     * 신규 아이디인지 확인한다.
     * @param userId 아이디
     * @return true 신규아이디 / false 중복 아이디
     */
    // id 중복확인을 참거짓으로 표현해라(id를 받아)
    // 중복검사를 했을 때 0보다 크면 돌려줘라
    private boolean isNewUserId (String userId) {
        long dupCount = memberRepository.countByUserId(userId);

        return dupCount <= 0;
    }

    // 중복확인
    public MemberDupCheckResponse getMemberIdDupCheck(String userId){
        MemberDupCheckResponse response = new MemberDupCheckResponse();
        response.setIsNew(isNewUserId(userId));

        return response;
    }

    public MemberCountResponse getMemberCountCheck(LocalDate joinDate){
        List<Member> memberList = memberRepository.countByJoinDate(joinDate);
        for (Member member: memberList){

        }
        MemberCountResponse response = new MemberCountResponse();
        response.setTodayTotal(getMembers().size());
        return response;
    }

    /**
     *
     * @return 전체 회원 조회
     */
    // 전체 회원 조회
    // 관리자는 처음에 MemberGroupType 변경 필요
    public List<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (Member member: originList){
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setUserId(member.getUserId());
            addItem.setUsername(member.getUsername());
            addItem.setJoinDate(member.getJoinDate().toLocalDate());
            addItem.setMemberGroupType(member.getMemberGroup().getPartType());

            result.add(addItem);
        }
        return result;
    }


    /**
     *
     * @param id 를 받아서
     * @return 회원 상세 조회(단수)
     */
    // 회원 정보 회원 ID별로 조회(단수)
    // WEB 관리자 화면 상세 보기
    public MemberResponse getMemberAdmin(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setUserId(originData.getUserId());
        response.setUsername(originData.getUsername());
        response.setBirthDate(originData.getBirthDate());
        response.setPassword(originData.getPassword());
        response.setJoinDate(originData.getJoinDate().toLocalDate());
        response.setMemberGroup(originData.getMemberGroup().getPartType());
        response.setMemberStatus(originData.getMemberStatus().getStatusType());

        return response;
    }

    /**
     *
     * @param id 를 받아서
     * @return 회원 상세 조회(단수)
     */
    // 회원 정보 회원 ID별로 조회(단수)
    // APP 화면 상세 보기
    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setUsername(originData.getUsername());
        response.setBirthDate(originData.getBirthDate());
        response.setPassword(originData.getPassword());

        return response;
    }


    /**
     *
     * @param id 회원 ID를 받아서
     * @param request username과 birthDate, password를 수정
     */
    // 회원정보 수정
    public void putMemberInfo(long id, MemberInfoChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setUsername(request.getUsername());
        originData.setBirthDate(request.getBirthDate());
        originData.setPassword(request.getPassword());

        memberRepository.save(originData);
    }


    /**
     *
     * @param id 탈퇴할 회원 ID를 받으면
     * @param request Member 상태를 탈퇴(AWAY), 탈퇴날짜(현재날짜)로 변경
     */

    // 회원 탈되(멤버 상태가 탈퇴일 때 로그인이 불가능한 로직 필요)
    public void putMemberOut(long id, MemberOutChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setMemberStatus(MemberStatus.AWAY);
        originData.setOutDate(LocalDateTime.now());

        memberRepository.save(originData);
    }
}
