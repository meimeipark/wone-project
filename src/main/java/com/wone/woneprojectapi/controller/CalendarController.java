package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Challenge;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.model.calendar.*;
import com.wone.woneprojectapi.service.CalendarService;
import com.wone.woneprojectapi.service.ChallengeService;
import com.wone.woneprojectapi.service.MemberService;
import com.wone.woneprojectapi.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/calendar")
public class CalendarController {
    private final CalendarService calendarService;
    private final MemberService memberService;
    private final ChallengeService challengeService;

    @PostMapping("/member-id/{memberId}/challenge-id/{challengeId}")
    public CommonResult setCalendar(@PathVariable long memberId, @PathVariable long challengeId ,@RequestBody CalendarCreateRequest request){
        Member member = memberService.getData(memberId);
        Challenge challenge = challengeService.getDataMoney(challengeId);
        calendarService.setCalendar(member, challenge,request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/one-day-spending/member-id/{memberId}")
    public ListResult<CalendarUserItem> getCalendarsUser(@PathVariable long memberId){
        Member member = memberService.getData(memberId);

        return ResponseService.getListResult(calendarService.getCalendarsUser(member));
    }

    @GetMapping("/one-day-spending/{id}")
    public SingleResult<CalendarResponse> getCalender(@PathVariable long id){
        return ResponseService.getSingleResult(calendarService.getCalendar(id));
    }

    @GetMapping("/statics/member-id/{memberId}/challenge-id/{challengeId}")
    public SingleResult<CalendarStaticResponse> getStatics(@PathVariable long memberId, @PathVariable long challengeId) throws Exception{
        Member member = memberService.getData(memberId);
        Challenge challenge = challengeService.getDataMoney(challengeId);
        return ResponseService.getSingleResult(calendarService.getStatics(member, challenge));
    }

    @PutMapping("/spending-change/{id}")
    public CommonResult putSpending(@PathVariable long id, CalendarSpendingChangeRequest request){
        calendarService.putSpending(id, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/spending/{id}")
    public CommonResult delSpending(@PathVariable long id){
        calendarService.delSpending(id);

        return ResponseService.getSuccessResult();
    }
}
