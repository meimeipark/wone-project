package com.wone.woneprojectapi.model.card;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CardDupCheckResponse {
    private Boolean cardCheck;
}
